package net.servodata.aucroc.service.content;


import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.ContentMapper;
import net.servodata.aucroc.api.persistence.SortOrder;
import net.servodata.aucroc.api.persistence.repository.content.ContentEntity;
import net.servodata.aucroc.api.persistence.repository.content.ContentFilter;
import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;
import net.servodata.aucroc.api.persistence.repository.content.ContentSort;
import net.servodata.aucroc.api.service.content.ContentCRUDService;
import net.servodata.aucroc.api.service.content.ContentDTO;

@Named
@Singleton
@OsgiServiceProvider(classes = {ContentCRUDService.class})
@Transactional
public class ContentCRUDServiceImpl implements ContentCRUDService
{
    private static final Logger LOG = LogManager.getLogger(ContentCRUDServiceImpl.class);

    @Inject
    @OsgiService
    private ContentRepository repository;

    @PostConstruct
    public void activate()
    {
        LOG.info("Running PostConstruct.");

        ContentDTO dto;

        dto = new ContentDTO();
        dto.setName("Content 1");
        dto.setDescription("Description of content 1");
        create(dto);

        dto = new ContentDTO();
        dto.setName("Content 2");
        dto.setDescription("Description of content 2");
        create(dto);

        dto = new ContentDTO();
        dto.setName("Content 3");
        dto.setDescription("Description of content 3");
        dto.setFeatured(true);
        create(dto);

        ContentFilter filterNormal = new ContentFilter();
        filterNormal.setFeatured(false);

        ContentFilter filterFeatured = new ContentFilter();
        filterFeatured.setFeatured(true);

        ContentSort sort = new ContentSort();
        sort.setId(SortOrder.DESC);

        int numNormal = repository.filter(filterNormal, sort).size();
        int numFeatured = repository.filter(filterFeatured, sort).size();

        LOG.info("Found " + numNormal + " standard items and " + numFeatured + " featured items.");
    }

    @Override
    public ContentDTO create(ContentDTO dto)
    {
        final ContentEntity entity = ContentMapper.map(dto);

        LOG.info("Persisting " + dto.toString() + " as entity " + entity.toString());

        final long insertId = repository.insert(entity);
        return findById(insertId);
    }

    @Override
    public void update(ContentDTO dto)
    {
        ContentEntity entity = repository.findById(dto.getId());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setCategoryId(dto.getCategoryId());
        entity.setImageFileId(dto.getImageFileId());
        entity.setFeatured(dto.isFeatured());
        repository.update(entity);

        dto.setUpdatedAt(entity.getUpdatedAt().toInstant());
    }

    @Override
    public void delete(ContentDTO dto)
    {
        repository.deleteById(dto.getId());
    }

    @Override
    public void deleteById(long id)
    {
        repository.deleteById(id);
    }

    private ContentDTO findById(long id)
    {
        return ContentMapper.map(repository.findById(id));
    }
}
