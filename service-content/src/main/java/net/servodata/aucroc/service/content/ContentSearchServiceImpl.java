package net.servodata.aucroc.service.content;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.ContentMapper;
import net.servodata.aucroc.api.persistence.SortOrder;
import net.servodata.aucroc.api.persistence.repository.content.ContentFilter;
import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;
import net.servodata.aucroc.api.persistence.repository.content.ContentSort;
import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.api.service.content.ContentSearchService;

@Named
@Singleton
@OsgiServiceProvider(classes = {ContentSearchService.class})
@Transactional
public class ContentSearchServiceImpl implements ContentSearchService
{
    private static final Logger LOG = LogManager.getLogger(ContentSearchServiceImpl.class);

    @Inject
    @OsgiService
    private ContentRepository repository;

    @Override
    public List<ContentDTO> search(String text)
    {
        final ContentFilter filter = new ContentFilter();
        filter.setFulltext(text);

        final ContentSort sort = new ContentSort();
        sort.setName(SortOrder.ASC);

        return repository.filter(filter, sort).stream()
                .map(ContentMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<ContentDTO> search(String text, int limit)
    {
        return search(text, limit, 0);
    }

    @Override
    public List<ContentDTO> search(String text, int limit, int offset)
    {
        final ContentFilter filter = new ContentFilter();
        filter.setFulltext(text);

        final ContentSort sort = new ContentSort();
        sort.setName(SortOrder.ASC);

        return repository.filter(limit, offset, filter, sort).stream()
                .map(ContentMapper::map)
                .collect(Collectors.toList());
    }
}
