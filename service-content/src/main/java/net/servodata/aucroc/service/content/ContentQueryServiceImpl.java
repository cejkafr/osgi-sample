package net.servodata.aucroc.service.content;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.ContentMapper;
import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;
import net.servodata.aucroc.api.service.content.ContentQueryService;
import net.servodata.aucroc.api.service.content.ContentDTO;

@Named
@Singleton
@OsgiServiceProvider(classes = {ContentQueryService.class})
@Transactional
public class ContentQueryServiceImpl implements ContentQueryService
{
    private static final Logger LOG = LogManager.getLogger(ContentQueryServiceImpl.class);

    @Inject
    @OsgiService
    private ContentRepository repository;

    @Override
    public ContentDTO findById(long id)
    {
        return ContentMapper.map(repository.findById(id));
    }

    @Override
    public List<ContentDTO> findAll()
    {
        return repository.findAll().stream()
                .map(ContentMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<ContentDTO> findAll(int limit, int offset)
    {
        return repository.findAll(limit, offset).stream()
                .map(ContentMapper::map)
                .collect(Collectors.toList());
    }
}
