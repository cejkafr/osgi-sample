package net.servodata.aucroc.web.page.home;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import net.servodata.aucroc.api.service.content.ContentWidgetService;
import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.web.page.LayoutTemplate;
import net.servodata.aucroc.web.page.show.ShowItemPage;

public class HomePage extends LayoutTemplate
{
    private static final Logger LOG = LogManager.getLogger(HomePage.class);

    @Inject
    private transient List<ContentWidgetService> contentWidgetServiceList;
    private transient ListView<ContentWidget> contentWidgetListView;

    @Override
    public void onInitialize()
    {
        super.onInitialize();

        add(contentWidgetListView = new ListView<ContentWidget>("widgets",
                new LoadableDetachableModel<List<ContentWidget>>()
        {
            @Override
            protected List<ContentWidget> load()
            {
                final List list = getContentWidgetList();
                LOG.info("Found " + list.size() + " content widgets.");
                return list;
            }
        })
        {
            @Override
            protected void populateItem(ListItem<ContentWidget> item)
            {
                item.add(new Label("name", item.getModel().getObject().getName()));
                //item.add(new Label("itemCount", item.getModel().getObject().getItems().size()));
                item.add(new ListView<ContentDTO>("items", item.getModel().getObject().getItems())
                {
                    @Override
                    protected void populateItem(ListItem<ContentDTO> item)
                    {
                        item.add(new StatelessLink<Void>("imgLink")
                        {
                            @Override
                            public void onClick()
                            {
                                PageParameters pageParameters = new PageParameters();
                                pageParameters.set("cid", item.getModel().getObject().getId());
                                setResponsePage(ShowItemPage.class, pageParameters);
                            }
                        }.add(new ExternalImage("img", "image/" + item.getModel().getObject().getImageFileId() + ".jpg")));

                        item.add(new StatelessLink<Void>("nameLink")
                        {
                            @Override
                            public void onClick()
                            {
                                PageParameters pageParameters = new PageParameters();
                                pageParameters.set("cid", item.getModel().getObject().getId());
                                setResponsePage(ShowItemPage.class, pageParameters);
                            }
                        }.add(new Label("name", new PropertyModel(item.getModel(), "name"))));
                    }
                });
            }
        });
    }

    private List<ContentWidget> getContentWidgetList()
    {
        if (contentWidgetServiceList == null)
            return null;

        final List<ContentWidgetService> l = new ArrayList(contentWidgetServiceList.size());
        l.addAll(contentWidgetServiceList);

        return l.stream().map(contentWidgetService ->
        {
            LOG.info("Processing widget " + contentWidgetService.getClass().getSimpleName());

            ContentWidget cw = new ContentWidget();
            cw.setName(contentWidgetService.getClass().getSimpleName());
            cw.setItems(contentWidgetService.getContent(10));

            return cw;

        }).collect(Collectors.toList());
    }

    public void setContentWidgetServiceList(List<ContentWidgetService> contentWidgetServiceList)
    {
        this.contentWidgetServiceList = contentWidgetServiceList;
    }
}
