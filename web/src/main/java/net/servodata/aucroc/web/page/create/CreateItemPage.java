package net.servodata.aucroc.web.page.create;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.servodata.aucroc.web.form.ContentItemForm;
import net.servodata.aucroc.web.page.LayoutTemplate;

public class CreateItemPage extends LayoutTemplate
{
    private static final Logger LOG = LogManager.getLogger(CreateItemPage.class);

    @Override
    public void onInitialize()
    {
        super.onInitialize();

        add(new ContentItemForm("newItem"));
    }
}
