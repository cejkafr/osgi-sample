package net.servodata.aucroc.web.panel.layout;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResourceReference;
import org.ops4j.pax.wicket.api.PaxWicketBeanAllowNull;

import net.servodata.aucroc.api.service.content.ContentSearchService;
import net.servodata.aucroc.web.page.home.HomePage;
import net.servodata.aucroc.web.page.home.SearchPage;

public class HeaderPanel extends Panel
{
    private static final Logger LOG = LogManager.getLogger(HeaderPanel.class);
    private static final String LOGO_IMAGE = "static/image/aucrock.png";

    private transient SearchForm form;

    public HeaderPanel(String id, PageParameters params)
    {
        super(id);

        final ContextRelativeResourceReference image =
                new ContextRelativeResourceReference(LOGO_IMAGE, true);

        final Link link = new StatelessLink<Void>("logoLink")
        {
            @Override
            public void onClick()
            {
                setResponsePage(HomePage.class);
            }
        };

        link.add(new Image("logoImg", image));

        add(link);
        add(form = new SearchForm("searchForm", params));
    }


    private final class SearchForm extends StatelessForm
    {
        @Inject
        @PaxWicketBeanAllowNull
        private transient ContentSearchService contentSearchService;

        private String q;

        public SearchForm(String id, PageParameters params)
        {
            super(id);

            setVersioned(false);
            setEnabled(contentSearchService != null);
            setDefaultModel(new CompoundPropertyModel(this));

            q = params.get("q").toString();

            final TextField qField = new RequiredTextField("q");
            add(qField);
        }

        @Override
        protected void onSubmit()
        {
            if (q == null || q.length() < 3)
                return;

            LOG.info("SEARCH {}", q);

            PageParameters params = new PageParameters();
            params.add("q", q);

            getSession().invalidateNow();
            setResponsePage(SearchPage.class, params);
        }

        public void setContentSearchService(ContentSearchService contentSearchService)
        {
            this.contentSearchService = contentSearchService;
        }
    }
}
