package net.servodata.aucroc.web.page.show;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.core.util.resource.WebExternalResourceStream;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.request.resource.ContextRelativeResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.request.resource.SharedResourceReference;
import org.apache.wicket.request.resource.UrlResourceReference;
import org.apache.wicket.resource.bundles.ResourceBundleReference;

import net.servodata.aucroc.api.service.category.CategoryDTO;
import net.servodata.aucroc.api.service.category.CategoryQueryService;
import net.servodata.aucroc.api.service.content.ContentQueryService;
import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.web.page.LayoutTemplate;
import net.servodata.aucroc.web.page.home.HomePage;
import net.servodata.aucroc.web.page.modify.ModifyItemPage;

public class ShowItemPage extends LayoutTemplate
{
    private static final Logger LOG = LogManager.getLogger(ShowItemPage.class);

    @Inject
    private transient ContentQueryService contentQueryService;
    @Inject
    private transient CategoryQueryService categoryQueryService;

    private long contentId;
    private ContentDTO contentDTO;
    private CategoryDTO categoryDTO;


    private Label category;
    private Label featured;
    private Label available;

    public ShowItemPage(PageParameters parameters)
    {
        super(parameters);

        if (parameters.get("cid").isNull() || parameters.get("cid").isEmpty()) {
            setResponsePage(HomePage.class);
        } else {
            contentId = parameters.get("cid").toLong();
            contentDTO = contentQueryService.findById(contentId);
            categoryDTO = (contentDTO.getCategoryId() == null ? null : categoryQueryService.findById(contentDTO.getCategoryId()));

            if (contentDTO == null || contentDTO.isDeleted()) {
                setResponsePage(HomePage.class);
            } else {
                add(new ExternalImage("image", "image/" + contentDTO.getImageFileId() + ".jpg"));
                add(new Label("name", contentDTO.getName()));
                add(category = new Label("category", ""));
                add(new Label("description", contentDTO.getDescription()));
                add(featured = new Label("featured", "FEATURED!"));
                add(available = new Label("available", "This item is not available anymore."));
                add(new Label("updatedAt", contentDTO.getUpdatedAt()));

                category.setVisible(categoryDTO != null);
                category.setDefaultModelObject((categoryDTO != null ? categoryDTO.getName() : ""));
                featured.setVisible(contentDTO.isFeatured());
                available.setVisible(!contentDTO.isAvailable());

                add(new StatelessLink("modLink") {
                    @Override
                    public void onClick() {
                        PageParameters pageParameters = new PageParameters();
                        pageParameters.set("cid", contentDTO.getId());
                        setResponsePage(ModifyItemPage.class, pageParameters);
                    }
                }.add(new Label("text", "Modify item")));
            }
        }
    }


    public void setContentQueryService(ContentQueryService contentQueryService)
    {
        this.contentQueryService = contentQueryService;
    }

    public void setCategoryQueryService(CategoryQueryService categoryQueryService) {
        this.categoryQueryService = categoryQueryService;
    }
}
