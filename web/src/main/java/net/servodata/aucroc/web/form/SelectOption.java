package net.servodata.aucroc.web.form;

import java.io.Serializable;

public class SelectOption implements Serializable
{
    public long value;
    public String label;

    public SelectOption()
    {
    }

    public SelectOption(long value)
    {
        this.value = value;
    }

    public SelectOption(long value, String label)
    {
        this.value = value;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
