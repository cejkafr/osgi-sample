package net.servodata.aucroc.web.panel.layout;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.ops4j.pax.wicket.api.PaxWicketBeanAllowNull;

import net.servodata.aucroc.api.service.category.CategoryQueryService;
import net.servodata.aucroc.api.service.category.CategoryDTO;

public class CategoryPanel extends Panel
{
    private static final Logger LOG = LogManager.getLogger(CategoryPanel.class);

    @Inject
    @PaxWicketBeanAllowNull
    private transient CategoryQueryService categoryQueryService;
    private transient ListView<CategoryDTO> categoryListView;

    public CategoryPanel(String id)
    {
        super(id);

        add(categoryListView = new ListView<CategoryDTO>("categoryListView",
                new LoadableDetachableModel<List<CategoryDTO>>()
                {
                    @Override
                    protected List<CategoryDTO> load()
                    {
                        final List list = getCategoryList();
                        LOG.info("Found " + list.size() + " categories.");
                        return list;
                    }
                })
        {
            @Override
            protected void populateItem(ListItem<CategoryDTO> item)
            {
                item.add(new Label("link", item.getModel().getObject().getName())
                {
                    /*@Override
                    public void onClick()
                    {

                    }*/

                    @Override
                    protected void onComponentTag(ComponentTag tag)
                    {
                        super.onComponentTag(tag);

                        tag.put("title", item.getModel().getObject().getDescription());
                    }
                });
            }
        });
    }

    private List<CategoryDTO> getCategoryList()
    {
        if (categoryQueryService == null) {
            LOG.warn("Category categoryQueryService NOT FOUND.");
            return Collections.EMPTY_LIST;
        }
        return categoryQueryService.findAll();
    }

    public void setCategoryQueryService(CategoryQueryService categoryQueryService)
    {
        this.categoryQueryService = categoryQueryService;
    }
}
