package net.servodata.aucroc.web.page.home;

import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.link.StatelessLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.ops4j.pax.wicket.api.PaxWicketBeanAllowNull;

import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.api.service.content.ContentSearchService;
import net.servodata.aucroc.web.page.LayoutTemplate;
import net.servodata.aucroc.web.page.show.ShowItemPage;

public class SearchPage extends LayoutTemplate
{
    private static final Logger LOG = LogManager.getLogger(SearchPage.class);

    @Inject
    @PaxWicketBeanAllowNull
    private transient ContentSearchService contentSearchService;

    private transient Component noResult;
    private transient Component listView;

    public SearchPage(PageParameters params)
    {
        super(params);

        final List<ContentDTO> results = contentSearchService.search(params.get("q").toString());
        LOG.info("Search found " + results + " items.");

        add(noResult = new Label("noResult", "Search for '" + params.get("q").toString() + "' found no results at all.").setVisible(false));
        add(listView = new ListView<ContentDTO>("items", results)
        {
            @Override
            protected void populateItem(ListItem<ContentDTO> item)
            {
                item.add(new StatelessLink<Void>("imgLink")
                {
                    @Override
                    public void onClick()
                    {
                        PageParameters pageParameters = new PageParameters();
                        pageParameters.set("cid", item.getModel().getObject().getId());
                        setResponsePage(ShowItemPage.class, pageParameters);
                    }
                }.add(new ExternalImage("img",
                        "https://f.aukro.cz/images/6886114340/400x300/e2411e8f44fbb13d91b9f079f14e")));

                item.add(new StatelessLink<Void>("nameLink")
                {
                    @Override
                    public void onClick()
                    {
                        PageParameters pageParameters = new PageParameters();
                        pageParameters.set("cid", item.getModel().getObject().getId());
                        setResponsePage(ShowItemPage.class, pageParameters);
                    }
                }.add(new Label("name", new PropertyModel(item.getModel(), "name"))));
            }
        });

        if (results == null) {
            setResponsePage(HomePage.class);
        } else if (results.isEmpty()) {
            noResult.setVisible(true);
            listView.setVisible(false);
        } else {
            noResult.setVisible(false);
            listView.setVisible(true);
        }
    }
}
