package net.servodata.aucroc.web.page.modify;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import net.servodata.aucroc.api.service.content.ContentQueryService;
import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.web.page.LayoutTemplate;
import net.servodata.aucroc.web.form.ContentItemForm;
import net.servodata.aucroc.web.page.home.HomePage;

public class ModifyItemPage extends LayoutTemplate
{
    private static final Logger LOG = LogManager.getLogger(ModifyItemPage.class);

    private final long contentId;
    private final ContentDTO content;

    @Inject
    private ContentQueryService contentQueryService;

    private ContentItemForm form;

    public ModifyItemPage(PageParameters parameters)
    {
        super(parameters);

        if (parameters.get("cid").isNull() || parameters.get("cid").isEmpty())
            setResponsePage(HomePage.class);

        contentId = parameters.get("cid").toLong();
        content = contentQueryService.findById(contentId);

        if (content == null || content.isDeleted())
            setResponsePage(HomePage.class);

        add(form = new ContentItemForm("modItem", content.getId()));

        form.setName(content.getName());
        form.setDescription(content.getDescription());
        form.setCategoryId(content.getCategoryId());
        form.setFeatured(content.isFeatured());
    }


    public void setContentQueryService(ContentQueryService contentQueryService)
    {
        this.contentQueryService = contentQueryService;
    }
}
