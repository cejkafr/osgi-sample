package net.servodata.aucroc.web.resource;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.request.resource.DynamicImageResource;
import org.ops4j.pax.wicket.api.InjectorHolder;

import net.servodata.aucroc.api.service.file.FileDTO;
import net.servodata.aucroc.api.service.file.FileQueryService;

public class ImageResource extends DynamicImageResource
{
    private static final Logger LOG = LogManager.getLogger(ImageResource.class);

    private byte[] imageNotFound;

    @Inject
    private FileQueryService fileQueryService;


    public ImageResource()
    {
        try {
            final InputStream is = getClass().getClassLoader().getResourceAsStream("static/image/not-found.jpg");
            imageNotFound = new byte[is.available()];
            is.read(imageNotFound, 0, is.available());
        } catch (IOException ex)
        {
            LOG.error(ex);
        }
    }

    public ImageResource(String format)
    {
        super(format);

        try {
            final InputStream is = getClass().getClassLoader().getResourceAsStream("static/image/not-found.jpg");
            imageNotFound = new byte[is.available()];
            is.read(imageNotFound, 0, is.available());
        } catch (IOException ex)
        {
            LOG.error(ex);
        }
    }

    public void setFileQueryService(FileQueryService fileQueryService)
    {
        this.fileQueryService = fileQueryService;
    }

    @Override
    protected byte[] getImageData(Attributes attributes)
    {
        InjectorHolder.getInjector().inject(this, ImageResource.class);

        final long fileId = parseId(attributes.getParameters().get("fileId").toString());
        final FileDTO dto = fileQueryService.findById(fileId);

        if (dto == null) {
            return imageNotFound;
        }

        return dto.getData();
    }

    private long parseId(String filename)
    {
        return Long.parseLong(filename.substring(0, filename.lastIndexOf(".")));
    }
}