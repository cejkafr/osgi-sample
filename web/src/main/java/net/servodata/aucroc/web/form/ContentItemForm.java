package net.servodata.aucroc.web.form;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.lang.Bytes;

import net.servodata.aucroc.api.service.category.CategoryQueryService;
import net.servodata.aucroc.api.service.content.ContentCRUDService;
import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.api.service.file.FileCRUDService;
import net.servodata.aucroc.api.service.file.FileDTO;
import net.servodata.aucroc.web.page.show.ShowItemPage;

public class ContentItemForm extends Form
{
    private static final Logger LOG = LogManager.getLogger(ContentItemForm.class);

    @Inject
    private transient ContentCRUDService contentCrudService;
    @Inject
    private transient CategoryQueryService categoryQueryService;
    @Inject
    private transient FileCRUDService fileCrudService;

    private transient Long itemId;
    private transient DropDownChoice<SelectOption> categoryDropDown;
    private transient FileUploadField fileUpload;

    private List<FileUpload> image;
    private String name;
    private String description;
    private SelectOption category;
    private boolean featured;

    public ContentItemForm(String id)
    {
        super(id);
        setDefaultModel(new CompoundPropertyModel(this));
        setMultiPart(true);
        setMaxSize(Bytes.kilobytes(1000));

        LOG.info("Creating form");

        add(fileUpload = new FileUploadField("image"));
        //fileUpload.setRequired(true);

        add(new RequiredTextField("name"));
        add(new TextArea("description").setRequired(true));
        categoryDropDown = new DropDownChoice("category");
        categoryDropDown.setChoiceRenderer(new ChoiceRenderer("label", "value"));
        categoryDropDown.setChoices(new LoadableDetachableModel<List<SelectOption>>()
        {
            @Override
            protected List<SelectOption> load()
            {
                return categoryQueryService.findAll().stream()
                        .map(dto -> new SelectOption(dto.getId(), dto.getName()))
                        .collect(Collectors.toList());
            }
        });
        categoryDropDown.setRequired(true);
        add(categoryDropDown);
        add(new CheckBox("featured"));

        LOG.info("Form created");
    }

    public ContentItemForm(String id, Long itemId)
    {
        super(id);
        this.itemId = itemId;
        setDefaultModel(new CompoundPropertyModel(this));
        setMultiPart(true);
        setMaxSize(Bytes.kilobytes(1000));

        LOG.info("Creating form");

        add(fileUpload = new FileUploadField("image"));
        //fileUpload.setRequired(true);

        add(new RequiredTextField("name"));
        add(new TextArea("description").setRequired(true));
        categoryDropDown = new DropDownChoice("category");
        categoryDropDown.setChoiceRenderer(new ChoiceRenderer("label", "value"));
        categoryDropDown.setChoices(new LoadableDetachableModel<List<SelectOption>>()
        {
            @Override
            protected List<SelectOption> load()
            {
                return categoryQueryService.findAll().stream()
                        .map(dto -> new SelectOption(dto.getId(), dto.getName()))
                        .collect(Collectors.toList());
            }
        });
        categoryDropDown.setRequired(true);
        add(categoryDropDown);
        add(new CheckBox("featured"));

        LOG.info("Form created");
    }

    @Override
    public final void onSubmit()
    {
        LOG.info("Subbmiting form");

        FileDTO fileDTO = null;
        if (!image.isEmpty()) {
            fileDTO = new FileDTO()
                    .setType("image")
                    .setData(image.get(0).getBytes());
            fileDTO = fileCrudService.create(fileDTO);
        }

        final ContentDTO dto = new ContentDTO()
                .setName(name)
                .setDescription(description)
                .setCategoryId(category.value)
                .setFeatured(featured);

        if (fileDTO != null) {
            dto.setImageFileId(fileDTO.getId());
        }

        final ContentDTO persistedDTO;

        if (itemId != null) {
            dto.setId(itemId);
            contentCrudService.update(dto);
            persistedDTO = dto;
            LOG.info("Updated new content of id #" + persistedDTO.getId());
        } else {
            persistedDTO = contentCrudService.create(dto);
            LOG.info("Persisted new content of id #" + persistedDTO.getId());
        }

        setResponsePage(ShowItemPage.class, new PageParameters().add("cid", persistedDTO.getId()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return category.value;
    }

    public void setCategoryId(long categoryId)
    {
        LOG.info("Setting category id");

        if (categoryDropDown != null && categoryDropDown.getChoices() != null) {
            final Optional opt = categoryDropDown.getChoices().stream()
                    .filter(o -> ((SelectOption) o).value == categoryId).findFirst();
            this.category = (opt.isPresent() ? (SelectOption) opt.get() : null);
        }
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public void setCategoryQueryService(CategoryQueryService categoryQueryService)
    {
        this.categoryQueryService = categoryQueryService;
    }

    public void setFileCrudService(FileCRUDService fileCrudService) {
        this.fileCrudService = fileCrudService;
    }

    public void setContentCrudService(ContentCRUDService contentCrudService) {
        this.contentCrudService = contentCrudService;
    }
}
