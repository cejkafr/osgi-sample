package net.servodata.aucroc.web.page.home;

import java.io.Serializable;
import java.util.List;

import net.servodata.aucroc.api.service.content.ContentDTO;

public class ContentWidget implements Serializable
{
    private String name;
    private List<ContentDTO> items;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ContentDTO> getItems() {
        return items;
    }

    public void setItems(List<ContentDTO> items) {
        this.items = items;
    }
}
