package net.servodata.aucroc.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.ResourceReference;

import net.servodata.aucroc.web.page.create.CreateItemPage;
import net.servodata.aucroc.web.page.home.HomePage;
import net.servodata.aucroc.web.page.home.SearchPage;
import net.servodata.aucroc.web.page.modify.ModifyItemPage;
import net.servodata.aucroc.web.page.show.ShowItemPage;
import net.servodata.aucroc.web.resource.ImageResource;

public class WicketApplication extends WebApplication
{
    private static final Logger LOG = LogManager.getLogger(WebApplication.class);

    @Override
    public void init()
    {
        super.init();

        mountPage("/add-item", CreateItemPage.class);
        mountPage("/mod-item", ModifyItemPage.class);
        mountPage("/show-item", ShowItemPage.class);
        mountPage("/search", SearchPage.class);

        final ResourceReference resourceReference = new ResourceReference("imageStreamer")
        {
            final ImageResource resource = new ImageResource();

            @Override
            public IResource getResource()
            {
                return resource;
            }
        };

        mountResource("/image/${fileId}", resourceReference);
    }


    @Override
    public RuntimeConfigurationType getConfigurationType()
    {
        return RuntimeConfigurationType.DEVELOPMENT;
    }

    @Override
    public Class<? extends WebPage> getHomePage()
    {
        return HomePage.class;
    }
}
