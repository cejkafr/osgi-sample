package net.servodata.aucroc.web.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.Component;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResourceReference;
import org.apache.wicket.settings.JavaScriptLibrarySettings;

import net.servodata.aucroc.web.panel.layout.CategoryPanel;
import net.servodata.aucroc.web.panel.layout.FooterPanel;
import net.servodata.aucroc.web.panel.layout.HeaderPanel;

public class LayoutTemplate extends WebPage
{
    private static final Logger LOG = LogManager.getLogger(LayoutTemplate.class);

    public static final String CONTENT_ID = "contentComponent";

    private HeaderPanel headerPanel;
    private Component categoriesPanel;
    private Component footerPanel;
    private Component feedbackPanel;

    public LayoutTemplate()
    {
        super();

        add(headerPanel = new HeaderPanel("headerPanel", new PageParameters()));
        add(categoriesPanel = new CategoryPanel("categoryPanel"));
        add(footerPanel = new FooterPanel("footerPanel"));
        add(feedbackPanel = new FeedbackPanel("feedbackPanel"));
    }

    public LayoutTemplate(PageParameters parameters)
    {
        super(parameters);

        add(headerPanel = new HeaderPanel("headerPanel", parameters));
        add(categoriesPanel = new CategoryPanel("categoryPanel"));
        add(footerPanel = new FooterPanel("footerPanel"));
        add(feedbackPanel = new FeedbackPanel("feedbackPanel"));
    }

    @Override
    public void onConfigure()
    {
        super.onConfigure();
    }

    @Override
    public void renderHead(IHeaderResponse response)
    {
        super.renderHead(response);

        final JavaScriptLibrarySettings javaScriptSettings = getApplication().getJavaScriptLibrarySettings();

        response.render(CssHeaderItem.forReference(new ContextRelativeResourceReference("static/css/bootstrap.min.css", false)));
        response.render(CssHeaderItem.forReference(new ContextRelativeResourceReference("static/css/desktop.css", true)));

        response.render(JavaScriptHeaderItem.forReference(javaScriptSettings.getJQueryReference()));
        response.render(JavaScriptHeaderItem.forUrl("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"));
        response.render(JavaScriptHeaderItem.forReference(new ContextRelativeResourceReference("static/js/bootstrap.min.js", false)));
    }

    public HeaderPanel getHeaderPanel()
    {
        return headerPanel;
    }
}
