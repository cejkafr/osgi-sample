package net.servodata.aucroc.service.content;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.servodata.aucroc.api.mapper.ContentMapper;
import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;
import net.servodata.aucroc.api.service.content.ContentDTO;
import net.servodata.aucroc.api.service.content.ContentSearchService;
import net.servodata.aucroc.service.content.request.MatchPhrasePrefix;
import net.servodata.aucroc.service.content.request.SearchQuery;
import net.servodata.aucroc.service.content.request.SearchRequest;
import net.servodata.aucroc.service.content.response.SearchResult;

@Component(service = ContentSearchService.class,
        property = {
                Constants.SERVICE_RANKING + "=1"
        })
public class ContentSearchServiceImpl implements ContentSearchService
{
    private static final Logger LOG = LogManager.getLogger(ContentSearchServiceImpl.class);

    private ContentRepository repository;

    @Reference
    public void setRepository(ContentRepository repository)
    {
        this.repository = repository;
    }

    @Override
    public List<ContentDTO> search(String text)
    {
        return searchInt(text);
    }

    @Override
    public List<ContentDTO> search(String text, int limit)
    {
        return search(text, limit, 0);
    }

    @Override
    public List<ContentDTO> search(String text, int limit, int offset)
    {
        return searchInt(text).subList(offset, limit);
    }

    private List<ContentDTO> searchInt(String q)
    {
        LOG.info("SEARCHING ELASTIC FOR " + q);
        LOG.info("OR SEARCHING ELASTIC FOR {}", q);

        final SearchRequest request = new SearchRequest(new SearchQuery(new MatchPhrasePrefix(q)));

        try {
            final URI uri = new URI("http://localhost:9200/content/item/_search"/*?q=" + URLEncoder.encode("%" + q + "%", "UTF-8")*/);
            final HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            final ObjectMapper mapper = new ObjectMapper();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("GET");
            //connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            OutputStream out = connection.getOutputStream();

            // write output

            LOG.info(mapper.writeValueAsString(request));

            mapper.writeValue(out, request);
            out.close();


            final InputStream is = connection.getInputStream();
            final SearchResult result = mapper.readValue(is, SearchResult.class);
            is.close();

            return result.getHits().getHits().stream()
                    .map(hit -> ContentMapper.map(hit.get_source()))
                    .collect(Collectors.toList());
        } catch (Exception e)
        {
            LOG.warn("Error sending event to rest service", e);
        }

        return Collections.EMPTY_LIST;
    }
}
