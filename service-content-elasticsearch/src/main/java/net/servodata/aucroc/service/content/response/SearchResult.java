package net.servodata.aucroc.service.content.response;

import java.io.Serializable;

public class SearchResult implements Serializable
{
    private int took;
    private boolean timed_out;
    private ContentResultShard _shards;
    private SearchResultHits hits;

    public SearchResult() {
    }

    public int getTook() {
        return took;
    }

    public void setTook(int took) {
        this.took = took;
    }

    public boolean isTimed_out() {
        return timed_out;
    }

    public void setTimed_out(boolean timed_out) {
        this.timed_out = timed_out;
    }

    public ContentResultShard get_shards() {
        return _shards;
    }

    public void set_shards(ContentResultShard _shards) {
        this._shards = _shards;
    }

    public SearchResultHits getHits() {
        return hits;
    }

    public void setHits(SearchResultHits hits) {
        this.hits = hits;
    }
}
