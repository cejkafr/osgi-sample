package net.servodata.aucroc.service.content.request;

import java.io.Serializable;

public class SearchQuery implements Serializable
{
    private MatchPhrasePrefix match_phrase_prefix;

    public SearchQuery()
    {
    }

    public SearchQuery(MatchPhrasePrefix match_phrase_prefix)
    {
        this.match_phrase_prefix = match_phrase_prefix;
    }

    public MatchPhrasePrefix getMatch_phrase_prefix()
    {
        return match_phrase_prefix;
    }

    public void setMatch_phrase_prefix(MatchPhrasePrefix match_phrase_prefix)
    {
        this.match_phrase_prefix = match_phrase_prefix;
    }
}
