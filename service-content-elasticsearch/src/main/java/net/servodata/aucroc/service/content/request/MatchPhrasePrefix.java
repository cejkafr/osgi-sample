package net.servodata.aucroc.service.content.request;

import java.io.Serializable;

public class MatchPhrasePrefix implements Serializable
{
    private String message;

    public MatchPhrasePrefix()
    {
    }

    public MatchPhrasePrefix(String match_phrase_prefix)
    {
        this.message = match_phrase_prefix;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
