package net.servodata.aucroc.service.content;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;

import org.apache.karaf.scheduler.Job;
import org.apache.karaf.scheduler.JobContext;
import org.apache.karaf.scheduler.Scheduler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;

@Component(immediate = true, property = {
        Scheduler.PROPERTY_SCHEDULER_EXPRESSION + "=0 0/1 * * * ?",
} )
public class ContentSync implements Job
{
    private static final Logger LOG = LogManager.getLogger(ContentSync.class);

    private ContentRepository repository;

    @Reference
    public void setRepository(ContentRepository repository)
    {
        this.repository = repository;
    }

    @Override
    public void execute(JobContext jobContext)
    {
        LOG.info("RUNNING SCHEDULED TASK");

        try {
            final URI uri = new URI("http://localhost:9200/content");
            final HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("DELETE");
            final InputStream is = connection.getInputStream();
            is.read();
            is.close();
        } catch (Exception e)
        {
            LOG.warn("Error sending event to rest service", e);
        }

        try {
            final URI uri = new URI("http://localhost:9200/content");
            final HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("PUT");
            final InputStream is = connection.getInputStream();
            is.read();
            is.close();
        } catch (Exception e)
        {
            LOG.warn("Error sending event to rest service", e);
        }

        final ObjectMapper mapper = new ObjectMapper();
        repository.findAll().stream().forEach(entity ->
        {
            try {

                final URI uri = new URI("http://localhost:9200/content/item/" + entity.getId());
                final HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
                connection.setDoOutput(true);
                connection.setInstanceFollowRedirects(false);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("charset", "utf-8");
                OutputStream out = connection.getOutputStream();

                // write output

                mapper.writeValue(out, entity);

                out.close();
                final InputStream is = connection.getInputStream();
                is.read();
                is.close();
            } catch (Exception e) {
                LOG.warn("Error sending event to rest service", e);
            }
        });
    }
}
