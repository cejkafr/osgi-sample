package net.servodata.aucroc.service.content.request;

import java.io.Serializable;

public class SearchRequest implements Serializable
{
    private SearchQuery query;

    public SearchRequest()
    {
    }

    public SearchRequest(SearchQuery query)
    {
        this.query = query;
    }

    public SearchQuery getQuery()
    {
        return query;
    }

    public void setQuery(SearchQuery query)
    {
        this.query = query;
    }
}
