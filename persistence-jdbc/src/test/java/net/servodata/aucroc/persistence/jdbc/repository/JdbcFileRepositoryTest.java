package net.servodata.aucroc.persistence.jdbc.repository;

import org.junit.BeforeClass;
import org.junit.Test;

import net.servodata.aucroc.api.persistence.repository.file.FileEntity;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JdbcFileRepositoryTest extends JdbcRepositoryTest
{
    private static JdbcFileRepository repository;

    @BeforeClass
    public static void initRepository()
    {
        repository = new JdbcFileRepository();
        repository.setEntityManager(getEntityManager());
    }

    @Test
    public void create()
    {
        final FileEntity createEntity = createEntity();

        getEntityManager().getTransaction().begin();
        final long entityId = repository.insert(createEntity);
        final FileEntity createdEntity = repository.findById(entityId);
        getEntityManager().getTransaction().rollback();

        assertEquals(createEntity.getType(), createdEntity.getType());
        assertArrayEquals(createEntity.getData(), createdEntity.getData());
        assertNotNull(createdEntity.getCreatedAt());
        assertNotNull(createdEntity.getUpdatedAt());
    }

    //@Test
    //public void update()
    //{
        //final List<CategoryEntity> entities = repository.findAll();
        //assertEquals("Find all result",0, entities.size());
    //}

    //@Test
    //public void delete()
    //{
        //final List<CategoryEntity> entities = repository.findAll();
        //assertEquals("Find all result",0, entities.size());
    //}

    //@Test
    //public void findAll()
    //{
       // final List<FileEntity> entities = repository.findById();
       // assertEquals("Find all result",0, entities.size());
    //}

    public FileEntity createEntity()
    {
        FileEntity entity = new FileEntity();
        entity.setType("image");
        entity.setData(new byte[] {'a', 'b', 'c'});
        return entity;
    }
}
