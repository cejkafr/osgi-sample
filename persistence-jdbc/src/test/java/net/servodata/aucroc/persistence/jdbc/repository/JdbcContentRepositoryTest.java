package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import net.servodata.aucroc.api.persistence.repository.content.ContentEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JdbcContentRepositoryTest extends JdbcRepositoryTest
{
    private static JdbcContentRepository repository;

    @BeforeClass
    public static void initRepository()
    {
        repository = new JdbcContentRepository();
        repository.setEntityManager(getEntityManager());
    }

    @Test
    public void create()
    {
        final ContentEntity createEntity = createEntity();

        getEntityManager().getTransaction().begin();
        final long entityId = repository.insert(createEntity);
        final ContentEntity createdEntity = repository.findById(entityId);
        getEntityManager().getTransaction().rollback();

        assertEquals(createEntity.getName(), createdEntity.getName());
        assertEquals(createEntity.getDescription(), createdEntity.getDescription());
        assertEquals(createEntity.getCategoryId(), createdEntity.getCategoryId());
        assertNotNull(createdEntity.getCreatedAt());
        assertNotNull(createdEntity.getUpdatedAt());
    }

    //@Test
    //public void update()
    //{
        //final List<CategoryEntity> entities = repository.findAll();
        //assertEquals("Find all result",0, entities.size());
    //}

    //@Test
    //public void delete()
    //{
        //final List<CategoryEntity> entities = repository.findAll();
        //assertEquals("Find all result",0, entities.size());
    //}

    @Test
    public void findAll()
    {
        final List<ContentEntity> entities = repository.findAll();
        assertEquals("Find all result",0, entities.size());
    }

    public ContentEntity createEntity()
    {
        ContentEntity entity = new ContentEntity();
        entity.setName("Category entity");
        entity.setDescription("Category entity description");
        entity.setCategoryId(null);
        return entity;
    }
}
