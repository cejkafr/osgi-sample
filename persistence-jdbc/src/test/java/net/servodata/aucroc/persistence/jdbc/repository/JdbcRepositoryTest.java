package net.servodata.aucroc.persistence.jdbc.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public abstract class JdbcRepositoryTest
{
    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    @BeforeClass
    public static void init()
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("managed-jpa-test");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @AfterClass
    public static void tearDown()
    {
        if (entityManager != null) {
            entityManager.clear();
            entityManager.close();
        }
        if (entityManagerFactory != null) {
            entityManagerFactory.close();
        }
    }

    public static EntityManager getEntityManager()
    {
        return entityManager;
    }
}
