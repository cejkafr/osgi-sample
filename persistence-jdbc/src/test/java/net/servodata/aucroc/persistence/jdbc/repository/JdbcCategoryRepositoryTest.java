package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import net.servodata.aucroc.api.persistence.repository.category.CategoryEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JdbcCategoryRepositoryTest extends JdbcRepositoryTest
{
    private static JdbcCategoryRepository repository;

    @BeforeClass
    public static void initRepository()
    {
        repository = new JdbcCategoryRepository();
        repository.setEntityManager(getEntityManager());
    }

    @Test
    public void create()
    {
        final CategoryEntity createEntity = createEntity();

        getEntityManager().getTransaction().begin();
        final long entityId = repository.insert(createEntity);
        final CategoryEntity createdEntity = repository.findById(entityId);
        getEntityManager().getTransaction().rollback();

        assertEquals(createEntity.getName(), createdEntity.getName());
        assertEquals(createEntity.getDescription(), createdEntity.getDescription());
        assertEquals(createEntity.getOrder(), createdEntity.getOrder());
        assertNotNull(createdEntity.getCreatedAt());
        assertNotNull(createdEntity.getUpdatedAt());
    }

    //@Test
    //public void update()
    //{
        //final List<CategoryEntity> entities = repository.findAll();
        //assertEquals("Find all result",0, entities.size());
    //}

    //@Test
    //public void delete()
    //{
        //final List<CategoryEntity> entities = repository.findAll();
        //assertEquals("Find all result",0, entities.size());
    //}

    @Test
    public void findAll()
    {
        final List<CategoryEntity> entities = repository.findAll();
        assertEquals("Find all result",0, entities.size());
    }

    public CategoryEntity createEntity()
    {
        CategoryEntity entity = new CategoryEntity();
        entity.setName("Category entity");
        entity.setDescription("Category entity description");
        entity.setOrder(105);
        return entity;
    }
}
