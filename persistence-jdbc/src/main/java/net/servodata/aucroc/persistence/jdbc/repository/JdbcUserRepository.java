package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.servodata.aucroc.api.persistence.repository.user.UserEntity;
import net.servodata.aucroc.api.persistence.repository.user.UserRepository;

public class JdbcUserRepository implements UserRepository
{
    private static final Logger LOG = LogManager.getLogger(JdbcUserRepository.class);

    @PersistenceContext(unitName="managed-jpa")
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    public long insert(UserEntity entity)
    {
        entity.setCreatedAt(Calendar.getInstance());
        entity.setUpdatedAt(entity.getCreatedAt());

        this.entityManager.persist(entity);
        this.entityManager.flush();

        return entity.getId();
    }

    @Override
    public void update(UserEntity entity)
    {
        entity.setUpdatedAt(entity.getCreatedAt());

        this.entityManager.persist(entity);
        this.entityManager.flush();
    }

    @Override
    public void delete(UserEntity entity)
    {
        entity.setDeleted(true);

        update(entity);
    }

    @Override
    public void deleteById(long entityId)
    {
        delete(findById(entityId));
    }

    @Override
    public UserEntity findById(long entityId)
    {
        return entityManager.find(UserEntity.class, entityId);
    }

    @Override
    public List<UserEntity> findAll()
    {
        final String query = "SELECT * FROM content WHERE deleted = false";
        return entityManager.createQuery(query).getResultList();
    }
}
