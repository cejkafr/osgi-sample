package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.servodata.aucroc.api.persistence.repository.category.CategoryEntity;
import net.servodata.aucroc.api.persistence.repository.category.CategoryFilter;
import net.servodata.aucroc.api.persistence.repository.category.CategoryRepository;
import net.servodata.aucroc.api.persistence.repository.category.CategorySort;

public class JdbcCategoryRepository implements CategoryRepository
{
    private static final Logger LOG = LogManager.getLogger(JdbcCategoryRepository.class);
    private static final String QUERY_BASE = "SELECT c FROM Category c";

    @PersistenceContext(unitName="managed-jpa")
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    public long insert(CategoryEntity entity)
    {
        entity.setDeleted(false);

        entity.setCreatedAt(Calendar.getInstance());
        entity.setUpdatedAt(entity.getCreatedAt());

        LOG.info("Persisting entity " + entity);

        entityManager.persist(entity);
        entityManager.flush();

        return entity.getId();
    }

    @Override
    public void update(CategoryEntity entity)
    {
        entity.setUpdatedAt(Calendar.getInstance());

        entityManager.merge(entity);
    }

    @Override
    public void delete(CategoryEntity entity)
    {
        entity.setDeleted(true);

        update(entity);
    }

    @Override
    public void deleteById(long entityId)
    {
        delete(findById(entityId));
    }

    @Override
    public CategoryEntity findById(long entityId)
    {
        return entityManager.find(CategoryEntity.class, entityId);
    }

    @Override
    public List<CategoryEntity> findAll()
    {
        final String query = QUERY_BASE + " WHERE deleted = false";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<CategoryEntity> findAll(int limit, int offset)
    {
        final String query = QUERY_BASE + " WHERE deleted = false";
        return entityManager.createQuery(query).setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    @Override
    public List<CategoryEntity> findAll(CategorySort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, null);
        appendSort(query,  sort);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.getResultList();
    }

    @Override
    public List<CategoryEntity> findAll(int limit, int offset, CategorySort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, null);
        appendSort(query,  sort);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    @Override
    public List<CategoryEntity> filter(CategoryFilter filter)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query,  null);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.getResultList();
    }

    @Override
    public List<CategoryEntity> filter(int limit, int offset, CategoryFilter filter)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query, null);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    @Override
    public List<CategoryEntity> filter(CategoryFilter filter, CategorySort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query, sort);

        LOG.info("Using query: " + query.toString());

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.getResultList();
    }

    @Override
    public List<CategoryEntity> filter(int limit, int offset, CategoryFilter filter, CategorySort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query, sort);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    private void appendFilter(StringBuilder query, List<Object> params, CategoryFilter filter)
    {
        int pos = 0;

        query.append(" WHERE deleted = ?").append(pos++);
        params.add(false);

        if (filter == null) {
            return;
        }

        for (String key : filter.getFilterMap().keySet()) {

            if (key.equals("fulltext")) {
                query.append(" AND ");
                query.append("c.name LIKE CONCAT('%',?").append(pos++).append(",'%')");
                params.add(filter.getFilterMap().get(key));
            } else {
                query.append(" AND ");
                query.append("c.").append(key).append(" = ?").append(pos++);
                params.add(filter.getFilterMap().get(key));
            }

            LOG.info("Adding filter c." + key + " = " + filter.getFilterMap().get(key));
        }
    }

    private void appendSort(StringBuilder query, CategorySort sort)
    {
        if (!sort.getSortMap().isEmpty()) {
            query.append(" ORDER BY ");

            int i = 0;
            for (String key : sort.getSortMap().keySet()) {

                if (i > 0) {
                    query.append(", ");
                }

                query.append("c.").append(key).append(" ").append(sort.getSortMap().get(key).toString());

                i++;
            }
        }
    }
}
