package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.servodata.aucroc.api.persistence.repository.file.FileEntity;
import net.servodata.aucroc.api.persistence.repository.file.FileRepository;

public class JdbcFileRepository implements FileRepository
{
    private static final Logger LOG = LogManager.getLogger(JdbcFileRepository.class);

    @PersistenceContext(unitName="managed-jpa")
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    public long insert(FileEntity entity)
    {
        entity.setCreatedAt(Calendar.getInstance());
        entity.setUpdatedAt(entity.getCreatedAt());

        LOG.info("Persisting entity " + entity);

        entityManager.persist(entity);
        entityManager.flush();

        return entity.getId();
    }

    @Override
    public void update(FileEntity entity)
    {
        entity.setUpdatedAt(Calendar.getInstance());

        entityManager.merge(entity);
    }

    @Override
    public void delete(FileEntity entity)
    {
        entityManager.remove(entity);
    }

    @Override
    public void deleteById(long entityId)
    {
        delete(findById(entityId));
    }

    @Override
    public FileEntity findById(long entityId)
    {
        return entityManager.find(FileEntity.class, entityId);
    }
}
