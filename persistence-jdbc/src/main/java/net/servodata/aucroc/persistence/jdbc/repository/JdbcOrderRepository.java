package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.servodata.aucroc.api.persistence.repository.order.OrderEntity;
import net.servodata.aucroc.api.persistence.repository.order.OrderRepository;

public class JdbcOrderRepository implements OrderRepository
{
    private static final Logger LOG = LogManager.getLogger(JdbcOrderRepository.class);

    @PersistenceContext(unitName="managed-jpa")
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    public long insert(OrderEntity entity) {
        return 0;
    }

    @Override
    public void update(OrderEntity entity)
    {
    }

    @Override
    public void delete(OrderEntity entity) {

    }

    @Override
    public void deleteById(long entityId) {

    }

    @Override
    public OrderEntity findById(long entityId) {
        return null;
    }

    @Override
    public List<OrderEntity> findAll() {
        return null;
    }
}
