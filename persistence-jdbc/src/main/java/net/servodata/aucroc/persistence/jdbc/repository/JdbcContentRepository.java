package net.servodata.aucroc.persistence.jdbc.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.servodata.aucroc.api.persistence.repository.content.ContentEntity;
import net.servodata.aucroc.api.persistence.repository.content.ContentFilter;
import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;
import net.servodata.aucroc.api.persistence.repository.content.ContentSort;

public class JdbcContentRepository implements ContentRepository
{
    private static final Logger LOG = LogManager.getLogger(JdbcContentRepository.class);
    private static final String QUERY_BASE = "SELECT c FROM Content c";

    @PersistenceContext(unitName="managed-jpa")
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    @Override
    public long insert(ContentEntity entity)
    {
        entity.setAvailable(true);
        entity.setDeleted(false);

        entity.setCreatedAt(Calendar.getInstance());
        entity.setUpdatedAt(entity.getCreatedAt());

        LOG.info("Persisting entity " + entity);

        entityManager.persist(entity);
        entityManager.flush();

        return entity.getId();
    }

    @Override
    public void update(ContentEntity entity)
    {
        entity.setUpdatedAt(Calendar.getInstance());

        entityManager.merge(entity);
    }

    @Override
    public void delete(ContentEntity entity)
    {
        entity.setDeleted(true);

        update(entity);
    }

    @Override
    public void deleteById(long entityId)
    {
        delete(findById(entityId));
    }

    @Override
    public ContentEntity findById(long entityId)
    {
        return entityManager.find(ContentEntity.class, entityId);
    }

    @Override
    public List<ContentEntity> findAll()
    {
        final String query = QUERY_BASE + " WHERE deleted = false";
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<ContentEntity> findAll(int limit, int offset)
    {
        final String query = QUERY_BASE + " WHERE deleted = false";
        return entityManager.createQuery(query).setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    @Override
    public List<ContentEntity> findAll(ContentSort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, null);
        appendSort(query,  sort);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.getResultList();
    }

    @Override
    public List<ContentEntity> findAll(int limit, int offset, ContentSort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, null);
        appendSort(query,  sort);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    @Override
    public List<ContentEntity> filter(ContentFilter filter)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query,  null);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.getResultList();
    }

    @Override
    public List<ContentEntity> filter(int limit, int offset, ContentFilter filter)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query, null);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    @Override
    public List<ContentEntity> filter(ContentFilter filter, ContentSort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query, sort);

        LOG.info("Using query: " + query.toString());

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.getResultList();
    }

    @Override
    public List<ContentEntity> filter(int limit, int offset, ContentFilter filter, ContentSort sort)
    {
        final StringBuilder query = new StringBuilder(QUERY_BASE);
        final List<Object> params = new ArrayList<>();

        appendFilter(query, params, filter);
        appendSort(query, sort);

        final Query q = entityManager.createQuery(query.toString());
        for (int i = 0; i < params.size(); i++) {
            q.setParameter(i, params.get(i));
        }

        return q.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    private void appendFilter(StringBuilder query, List<Object> params, ContentFilter filter)
    {
        int pos = 0;

        query.append(" WHERE deleted = ?").append(pos++);
        params.add(false);

        if (filter == null) {
            return;
        }

        for (String key : filter.getFilterMap().keySet()) {

            if (key.equals("fulltext")) {
                query.append(" AND ");
                query.append("c.name LIKE CONCAT('%',?").append(pos++).append(",'%')");
                params.add(filter.getFilterMap().get(key));
            } else {
                query.append(" AND ");
                query.append("c.").append(key).append(" = ?").append(pos++);
                params.add(filter.getFilterMap().get(key));
            }

            LOG.info("Adding filter c." + key + " = " + filter.getFilterMap().get(key));
        }
    }

    private void appendSort(StringBuilder query, ContentSort sort)
    {
        if (!sort.getSortMap().isEmpty()) {
            query.append(" ORDER BY ");

            int i = 0;
            for (String key : sort.getSortMap().keySet()) {

                if (i > 0) {
                    query.append(", ");
                }

                query.append("c.").append(key).append(" ").append(sort.getSortMap().get(key).toString());

                i++;
            }
        }
    }
}
