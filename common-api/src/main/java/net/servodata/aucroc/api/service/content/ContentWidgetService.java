package net.servodata.aucroc.api.service.content;

import java.util.List;

public interface ContentWidgetService
{
    List<ContentDTO> getContent(int limit);
}
