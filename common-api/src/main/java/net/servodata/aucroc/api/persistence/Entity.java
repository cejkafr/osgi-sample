package net.servodata.aucroc.api.persistence;

import java.io.Serializable;
import java.util.Calendar;

public interface Entity extends Serializable
{
    long getId();

    Calendar getCreatedAt();
    Calendar getUpdatedAt();
}
