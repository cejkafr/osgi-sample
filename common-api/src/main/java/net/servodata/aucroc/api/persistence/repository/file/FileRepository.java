package net.servodata.aucroc.api.persistence.repository.file;

import net.servodata.aucroc.api.persistence.CRUDRepository;

public interface FileRepository extends CRUDRepository<FileEntity>
{
    FileEntity findById(long entityId);
}
