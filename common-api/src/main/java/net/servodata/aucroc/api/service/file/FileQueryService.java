package net.servodata.aucroc.api.service.file;

import net.servodata.aucroc.api.service.QueryService;

public interface FileQueryService extends QueryService<FileDTO>
{
}
