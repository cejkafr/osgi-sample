package net.servodata.aucroc.api.service.category;

import net.servodata.aucroc.api.service.QueryService;

public interface CategoryQueryService extends QueryService<CategoryDTO>
{
}
