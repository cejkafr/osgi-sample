package net.servodata.aucroc.api.persistence.repository.user;

import java.util.Calendar;
import java.util.List;

import net.servodata.aucroc.api.persistence.Entity;
import net.servodata.aucroc.api.persistence.repository.content.ContentEntity;

public class UserEntity implements Entity
{
    private long id;
    private String email;
    private String name;
    private boolean active;
    private boolean deleted;
    private Calendar createdAt;
    private Calendar updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    public Calendar getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Calendar updatedAt) {
        this.updatedAt = updatedAt;
    }
}
