package net.servodata.aucroc.api.persistence;

import javax.persistence.EntityManager;

public interface Repository
{
    void setEntityManager(EntityManager entityManager);
}
