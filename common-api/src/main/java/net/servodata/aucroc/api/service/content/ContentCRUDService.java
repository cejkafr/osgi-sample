package net.servodata.aucroc.api.service.content;

import net.servodata.aucroc.api.service.CRUDService;

public interface ContentCRUDService extends CRUDService<ContentDTO>
{
}
