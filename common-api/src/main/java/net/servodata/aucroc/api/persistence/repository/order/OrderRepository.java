package net.servodata.aucroc.api.persistence.repository.order;

import java.util.List;

import net.servodata.aucroc.api.persistence.CRUDRepository;

public interface OrderRepository extends CRUDRepository<OrderEntity>
{
    OrderEntity findById(long entityId);
    List<OrderEntity> findAll();
}
