package net.servodata.aucroc.api.persistence.repository.category;

import java.util.List;

import net.servodata.aucroc.api.persistence.CRUDRepository;

public interface CategoryRepository extends CRUDRepository<CategoryEntity>
{
    CategoryEntity findById(long entityId);

    List<CategoryEntity> findAll();
    List<CategoryEntity> findAll(CategorySort sort);
    List<CategoryEntity> findAll(int limit, int offset);
    List<CategoryEntity> findAll(int limit, int offset, CategorySort sort);

    List<CategoryEntity> filter(CategoryFilter filter);
    List<CategoryEntity> filter(int limit, int offset, CategoryFilter filter);

    List<CategoryEntity> filter(CategoryFilter filter, CategorySort sort);
    List<CategoryEntity> filter(int limit, int offset, CategoryFilter filter, CategorySort sort);
}
