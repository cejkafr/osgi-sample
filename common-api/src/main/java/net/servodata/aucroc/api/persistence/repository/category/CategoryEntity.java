package net.servodata.aucroc.api.persistence.repository.category;

import java.util.Calendar;
import java.util.List;

import net.servodata.aucroc.api.persistence.Entity;
import net.servodata.aucroc.api.persistence.repository.content.ContentEntity;

public class CategoryEntity implements Entity
{
    private long id;
    private String name;
    private String description;
    private int order;
    private boolean available;
    private boolean deleted;
    private Calendar createdAt;
    private Calendar updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    public Calendar getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Calendar updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "CategoryEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", order=" + order +
                ", available=" + available +
                ", deleted=" + deleted +
                ", createdAt=" + (createdAt == null ? "null" : createdAt.toInstant()) +
                ", updatedAt=" + (updatedAt == null ? "null" : updatedAt.toInstant()) +
                '}';
    }
}
