package net.servodata.aucroc.api.service.category;

import java.time.Instant;

import net.servodata.aucroc.api.service.DataObject;
import net.servodata.aucroc.api.service.user.UserDTO;

public class CategoryDTO implements DataObject
{
    private long id;
    private String name;
    private String description;
    private int order;
    private boolean available;
    private boolean deleted;
    private UserDTO createdBy;
    private Instant createdAt;
    private Instant updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public CategoryDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CategoryDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CategoryDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getOrder() {
        return order;
    }

    public CategoryDTO setOrder(int order) {
        this.order = order;
        return this;
    }

    public boolean isAvailable() {
        return available;
    }

    public CategoryDTO setAvailable(boolean available) {
        this.available = available;
        return this;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public CategoryDTO setDeleted(boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public UserDTO getCreatedBy() {
        return createdBy;
    }

    public CategoryDTO setCreatedBy(UserDTO createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public CategoryDTO setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public CategoryDTO setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", order=" + order +
                ", available=" + available +
                ", deleted=" + deleted +
                ", createdBy=" + createdBy +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
