package net.servodata.aucroc.api.service.content;

import java.time.Instant;

import net.servodata.aucroc.api.service.DataObject;
import net.servodata.aucroc.api.service.category.CategoryDTO;
import net.servodata.aucroc.api.service.file.FileDTO;
import net.servodata.aucroc.api.service.user.UserDTO;

public class ContentDTO implements DataObject
{
    private long id;
    private String name;
    private String description;
    private long categoryId;
    private long imageFileId;
    private long createdByUserId;
    private boolean available;
    private boolean featured;
    private boolean deleted;
    private UserDTO createdBy;
    private Instant createdAt;
    private Instant updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public ContentDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ContentDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ContentDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public ContentDTO setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public Long getImageFileId() {
        return imageFileId;
    }

    public ContentDTO setImageFileId(Long image) {
        this.imageFileId = image;
        return this;
    }

    public Long getCreatedByUserId() {
        return createdByUserId;
    }

    public ContentDTO setCreatedByUserId(Long createdByUserId) {
        this.createdByUserId = createdByUserId;
        return this;
    }

    public boolean isAvailable() {
        return available;
    }

    public ContentDTO setAvailable(boolean available) {
        this.available = available;
        return this;
    }

    public boolean isFeatured() {
        return featured;
    }

    public ContentDTO setFeatured(boolean featured) {
        this.featured = featured;
        return this;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public ContentDTO setDeleted(boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public UserDTO getCreatedBy() {
        return createdBy;
    }

    public ContentDTO setCreatedBy(UserDTO createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public ContentDTO setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public ContentDTO setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @Override
    public String toString() {
        return "ContentDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", available=" + available +
                ", featured=" + featured +
                ", deleted=" + deleted +
                ", createdBy=" + createdBy +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
