package net.servodata.aucroc.api.persistence.repository.order;

import java.util.Calendar;

import net.servodata.aucroc.api.persistence.Entity;

public class OrderEntity implements Entity
{
    private long id;
    private Calendar createdAt;
    private Calendar updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    public Calendar getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Calendar updatedAt) {
        this.updatedAt = updatedAt;
    }
}
