package net.servodata.aucroc.api.persistence.repository.category;

import java.util.Calendar;
import java.util.List;

import net.servodata.aucroc.api.persistence.AbstractFilter;

public class CategoryFilter extends AbstractFilter
{
    public List<Long> getId() {
        return (List) filterMap.getOrDefault("id", null);
    }

    public void setId(List<Long> value) {
        filterMap.put("id", value);
    }

    public String getName() {
        return (String) filterMap.getOrDefault("name", null);
    }

    public void setName(String value) {
        filterMap.put("name", value);
    }

    public String getFulltext() {
        return (String) filterMap.getOrDefault("fulltext", null);
    }

    public void setFulltext(String value) {
        filterMap.put("fulltext", value);
    }

    public boolean isAvailable() {
        return (Boolean) filterMap.getOrDefault("available", null);
    }

    public void setAvailable(boolean value) {
        filterMap.put("available", value);
    }

    public boolean isDeleted() {
        return (Boolean) filterMap.getOrDefault("deleted", null);
    }

    public void setDeleted(boolean value) {
        filterMap.put("deleted", value);
    }

    public Calendar getCreatedAt() {
        return (Calendar) filterMap.getOrDefault("createdAt", null);
    }

    public void setCreatedAt(Calendar value) {
        filterMap.put("createdAt", value);
    }

    public Calendar getUpdatedAt() {
        return (Calendar) filterMap.getOrDefault("updatedAt", null);
    }

    public void setUpdatedAt(Calendar value) {
        filterMap.put("updatedAt", value);
    }
}
