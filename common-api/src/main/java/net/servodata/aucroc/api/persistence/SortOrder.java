package net.servodata.aucroc.api.persistence;

public enum SortOrder
{
    ASC, DESC;
}
