package net.servodata.aucroc.api.mapper;

import net.servodata.aucroc.api.persistence.repository.category.CategoryEntity;
import net.servodata.aucroc.api.service.category.CategoryDTO;

public abstract class CategoryMapper
{
    public static CategoryDTO map(CategoryEntity entity)
    {
        if (entity == null)
            return null;

        return new CategoryDTO()
                .setId(entity.getId())
                .setName(entity.getName())
                .setDescription(entity.getDescription())
                .setOrder(entity.getOrder())
                .setAvailable(entity.isAvailable())
                .setDeleted(entity.isDeleted())
                .setCreatedAt(entity.getCreatedAt().toInstant())
                .setUpdatedAt(entity.getUpdatedAt().toInstant());
    }

    public static CategoryEntity map(CategoryDTO dto)
    {
        if (dto == null)
            return null;

        final CategoryEntity entity = new CategoryEntity();
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setOrder(dto.getOrder());
        entity.setAvailable(dto.isAvailable());
        return entity;
    }
}
