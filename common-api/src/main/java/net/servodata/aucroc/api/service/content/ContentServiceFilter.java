package net.servodata.aucroc.api.service.content;

import java.time.Instant;
import java.util.Calendar;
import java.util.List;

public class ContentServiceFilter
{
    private List<Long> id;
    private String name;
    private Instant createdAt;
    private Instant updatedAt;

    public List<Long> getId() {
        return id;
    }

    public void setId(List<Long> id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }
}
