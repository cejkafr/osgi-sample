package net.servodata.aucroc.api.service;

import java.util.List;

import net.servodata.aucroc.api.service.category.CategoryDTO;

public interface SearchService<T extends DataObject>
{
    List<T> search(String text);
    List<T> search(String text, int limit);
    List<T> search(String text, int limit, int offset);
}
