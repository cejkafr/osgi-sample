package net.servodata.aucroc.api.persistence.repository.category;

import net.servodata.aucroc.api.persistence.AbstractSort;
import net.servodata.aucroc.api.persistence.SortOrder;

public class CategorySort extends AbstractSort
{
    public SortOrder getId() {
        return sortMap.getOrDefault("id", null);
    }

    public void setId(SortOrder order) {
        sortMap.put("id", order);
    }

    public SortOrder getName() {
        return sortMap.getOrDefault("name", null);
    }

    public void setName(SortOrder order) {
        sortMap.put("name", order);
    }

    public SortOrder getOrder() {
        return sortMap.getOrDefault("order", null);
    }

    public void setOrder(SortOrder order) {
        sortMap.put("order", order);
    }

    public SortOrder getCreatedAt() {
        return sortMap.getOrDefault("createdAt", null);
    }

    public void setCreatedAt(SortOrder order) {
        sortMap.put("createdAt", order);
    }

    public SortOrder getUpdatedAt() {
        return sortMap.getOrDefault("updatedAt", null);
    }

    public void setUpdatedAt(SortOrder order) {
        sortMap.put("updatedAt", order);
    }
}
