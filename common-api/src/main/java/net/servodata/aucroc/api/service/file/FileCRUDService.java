package net.servodata.aucroc.api.service.file;

import net.servodata.aucroc.api.service.CRUDService;

public interface FileCRUDService extends CRUDService<FileDTO>
{

}
