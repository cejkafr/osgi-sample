package net.servodata.aucroc.api.persistence.repository.file;

import java.util.Arrays;
import java.util.Calendar;

import net.servodata.aucroc.api.persistence.Entity;

public class FileEntity implements Entity
{
    private long id;
    private byte[] data;
    private String type;
    private Calendar createdAt;
    private Calendar updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Calendar getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Calendar updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "FileEntity{" +
                "id=" + id +
                ", data=" + Arrays.toString(data) +
                ", type='" + type + '\'' +
                ", createdAt=" + (createdAt == null ? "null" : createdAt.toInstant()) +
                ", updatedAt=" + (updatedAt == null ? "null" : updatedAt.toInstant()) +
                '}';
    }
}
