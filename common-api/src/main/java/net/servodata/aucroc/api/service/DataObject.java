package net.servodata.aucroc.api.service;

import java.io.Serializable;

public interface DataObject extends Serializable
{
    long getId();
}
