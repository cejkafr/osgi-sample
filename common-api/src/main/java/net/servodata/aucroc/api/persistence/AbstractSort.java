package net.servodata.aucroc.api.persistence;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractSort
{
    protected Map<String, SortOrder> sortMap;

    public AbstractSort()
    {
        this.sortMap = new HashMap<>();
    }

    public Map<String, SortOrder> getSortMap()
    {
        return sortMap;
    }
}
