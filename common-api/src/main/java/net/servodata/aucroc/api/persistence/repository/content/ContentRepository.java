package net.servodata.aucroc.api.persistence.repository.content;

import java.util.List;

import net.servodata.aucroc.api.persistence.CRUDRepository;

public interface ContentRepository extends CRUDRepository<ContentEntity>
{
    ContentEntity findById(long entityId);

    List<ContentEntity> findAll();
    List<ContentEntity> findAll(ContentSort sort);
    List<ContentEntity> findAll(int limit, int offset);
    List<ContentEntity> findAll(int limit, int offset, ContentSort sort);

    List<ContentEntity> filter(ContentFilter filter);
    List<ContentEntity> filter(int limit, int offset, ContentFilter filter);

    List<ContentEntity> filter(ContentFilter filter, ContentSort sort);
    List<ContentEntity> filter(int limit, int offset, ContentFilter filter, ContentSort sort);
}
