package net.servodata.aucroc.api.service.file;

import java.time.Instant;
import java.util.Arrays;

import net.servodata.aucroc.api.service.DataObject;

public class FileDTO implements DataObject
{
    private long id;
    private String type;
    private byte[] data;
    private Instant createdAt;
    private Instant updatedAt;

    @Override
    public long getId() {
        return id;
    }

    public FileDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public FileDTO setType(String type) {
        this.type = type;
        return this;
    }

    public byte[] getData() {
        return data;
    }

    public FileDTO setData(byte[] data) {
        this.data = data;
        return this;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public FileDTO setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public FileDTO setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @Override
    public String toString() {
        return "FileDTO{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", data=" + Arrays.toString(data) +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
