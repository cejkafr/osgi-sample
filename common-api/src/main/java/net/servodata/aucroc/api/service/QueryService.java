package net.servodata.aucroc.api.service;

import java.util.List;

public interface QueryService<T extends DataObject>
{
    T findById(long id);
    List<T> findAll();
    List<T> findAll(int limit, int offset);
}
