package net.servodata.aucroc.api.service;

public interface CRUDService<T extends DataObject>
{
    T create(T dto);
    void update(T dto);
    void delete(T dto);
    void deleteById(long id);
}
