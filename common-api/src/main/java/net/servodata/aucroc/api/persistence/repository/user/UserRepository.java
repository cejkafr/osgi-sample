package net.servodata.aucroc.api.persistence.repository.user;

import java.util.List;

import net.servodata.aucroc.api.persistence.CRUDRepository;

public interface UserRepository extends CRUDRepository<UserEntity>
{
    UserEntity findById(long entityId);
    List<UserEntity> findAll();
}
