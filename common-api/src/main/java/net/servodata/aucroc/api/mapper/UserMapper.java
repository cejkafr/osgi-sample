package net.servodata.aucroc.api.mapper;

import net.servodata.aucroc.api.persistence.repository.user.UserEntity;
import net.servodata.aucroc.api.service.user.UserDTO;

public abstract class UserMapper
{
    public static UserDTO map(UserEntity entity)
    {
        if (entity == null)
            return null;

        return new UserDTO()
                .setId(entity.getId())
                .setEmail(entity.getEmail())
                .setName(entity.getName())
                .setActive(entity.isActive())
                .setDeleted(entity.isDeleted())
                .setCreatedAt(entity.getCreatedAt().toInstant())
                .setUpdatedAt(entity.getUpdatedAt().toInstant());
    }

    public static UserEntity map(UserDTO dto)
    {
        if (dto == null)
            return null;

        final UserEntity entity = new UserEntity();
        entity.setEmail(dto.getEmail());
        entity.setName(dto.getName());
        return entity;
    }
}
