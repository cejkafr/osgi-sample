package net.servodata.aucroc.api.persistence;

public interface CRUDRepository<T extends Entity> extends Repository
{
    long insert(T entity);
    void update(T entity);
    void delete(T entity);

    void deleteById(long entityId);
}
