package net.servodata.aucroc.api.service.content;

import java.util.Map;

import net.servodata.aucroc.api.persistence.SortOrder;

public class ContentServiceSort
{
    private Map<String, SortOrder> sortMap;

    public SortOrder getId() {
        return sortMap.getOrDefault("id", null);
    }

    public void setId(SortOrder order) {
        sortMap.put("id", order);
    }

    public SortOrder getName() {
        return sortMap.getOrDefault("name", null);
    }

    public void setName(SortOrder order) {
        sortMap.put("name", order);
    }

    public SortOrder getCreatedAt() {
        return sortMap.getOrDefault("createdAt", null);
    }

    public void setCreatedAt(SortOrder order) {
        sortMap.put("createdAt", order);
    }

    public SortOrder getUpdatedAt() {
        return sortMap.getOrDefault("updatedAt", null);
    }

    public void setUpdatedAt(SortOrder order) {
        sortMap.put("updatedAt", order);
    }

    public Map<String, SortOrder> getSortMap() {
        return sortMap;
    }
}
