package net.servodata.aucroc.api.mapper;

import net.servodata.aucroc.api.persistence.repository.content.ContentEntity;
import net.servodata.aucroc.api.service.content.ContentDTO;

public abstract class ContentMapper
{
    public static ContentDTO map(ContentEntity entity)
    {
        if (entity == null)
            return null;

        return new ContentDTO()
            .setId(entity.getId())
            .setName(entity.getName())
            .setDescription(entity.getDescription())
            .setCategoryId(entity.getCategoryId())
            .setImageFileId(entity.getImageFileId())
            .setCreatedByUserId(entity.getCreatedByUserId())
            .setAvailable(entity.isAvailable())
            .setFeatured(entity.isFeatured())
            .setDeleted(entity.isDeleted())
            .setCreatedAt(entity.getCreatedAt().toInstant())
            .setUpdatedAt(entity.getUpdatedAt().toInstant());
    }

    public static ContentEntity map(ContentDTO dto)
    {
        if (dto == null)
            return null;

        final ContentEntity entity = new ContentEntity();
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setCategoryId(dto.getCategoryId());
        entity.setImageFileId(dto.getImageFileId());
        entity.setCreatedByUserId(dto.getCreatedByUserId());
        entity.setFeatured(dto.isFeatured());
        return entity;
    }
}
