package net.servodata.aucroc.api.service.content;

import net.servodata.aucroc.api.service.SearchService;

public interface ContentSearchService extends SearchService<ContentDTO>
{
}
