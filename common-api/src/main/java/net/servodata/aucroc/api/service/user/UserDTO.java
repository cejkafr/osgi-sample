package net.servodata.aucroc.api.service.user;

import java.time.Instant;
import java.util.List;

import net.servodata.aucroc.api.service.content.ContentDTO;

public class UserDTO
{
    private long id;
    private String email;
    private String name;
    private boolean active;
    private boolean deleted;
    private Instant createdAt;
    private Instant updatedAt;

    public long getId() {
        return id;
    }

    public UserDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDTO setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public UserDTO setActive(boolean active) {
        this.active = active;
        return this;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public UserDTO setDeleted(boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public UserDTO setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public UserDTO setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
}
