package net.servodata.aucroc.api.persistence;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractFilter
{
    protected Map<String, Object> filterMap;

    public AbstractFilter()
    {
        this.filterMap = new HashMap<>();
    }

    public Map<String, Object> getFilterMap()
    {
        return filterMap;
    }
}
