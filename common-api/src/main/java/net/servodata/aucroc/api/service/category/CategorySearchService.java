package net.servodata.aucroc.api.service.category;

import net.servodata.aucroc.api.service.SearchService;

public interface CategorySearchService extends SearchService<CategoryDTO>
{
}
