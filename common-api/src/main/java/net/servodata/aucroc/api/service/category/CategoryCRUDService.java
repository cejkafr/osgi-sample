package net.servodata.aucroc.api.service.category;

import net.servodata.aucroc.api.service.CRUDService;

public interface CategoryCRUDService extends CRUDService<CategoryDTO>
{

}
