package net.servodata.aucroc.api.service.content;

import net.servodata.aucroc.api.service.QueryService;

public interface ContentQueryService extends QueryService<ContentDTO>
{
}
