package net.servodata.aucroc.api.mapper;

import net.servodata.aucroc.api.persistence.repository.file.FileEntity;
import net.servodata.aucroc.api.service.file.FileDTO;

public abstract class FileMapper
{
    public static FileDTO map(FileEntity entity)
    {
        if (entity == null)
            return null;

        return new FileDTO()
                .setId(entity.getId())
                .setType(entity.getType())
                .setData(entity.getData())
                .setCreatedAt(entity.getCreatedAt().toInstant())
                .setUpdatedAt(entity.getUpdatedAt().toInstant());
    }

    public static FileEntity map(FileDTO dto)
    {
        if (dto == null)
            return null;

        final FileEntity entity = new FileEntity();
        entity.setType(dto.getType());
        entity.setData(dto.getData());
        return entity;
    }
}
