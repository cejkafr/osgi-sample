package net.servodata.aucroc.service.content;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import net.servodata.aucroc.api.mapper.ContentMapper;
import net.servodata.aucroc.api.persistence.SortOrder;
import net.servodata.aucroc.api.persistence.repository.content.ContentFilter;
import net.servodata.aucroc.api.persistence.repository.content.ContentRepository;
import net.servodata.aucroc.api.persistence.repository.content.ContentSort;
import net.servodata.aucroc.api.service.content.ContentWidgetService;
import net.servodata.aucroc.api.service.content.ContentDTO;

@Component(service = ContentWidgetService.class)
public class FeaturedContentWidgetServiceImpl implements ContentWidgetService
{
    private static final Logger LOG = LogManager.getLogger(FeaturedContentWidgetServiceImpl.class);

    private ContentRepository repository;

    @Reference
    public void setRepository(ContentRepository repository)
    {
        this.repository = repository;
    }

    @Override
    public List<ContentDTO> getContent(int limit)
    {
        final ContentFilter filter = new ContentFilter();
        filter.setFeatured(true);

        final ContentSort sort = new ContentSort();
        sort.setCreatedAt(SortOrder.ASC);

        return repository.filter(limit, 0, filter, sort)
                .stream().map(ContentMapper::map)
                .collect(Collectors.toList());
    }
}
