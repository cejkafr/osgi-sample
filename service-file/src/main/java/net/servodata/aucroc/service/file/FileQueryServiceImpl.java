package net.servodata.aucroc.service.file;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.FileMapper;
import net.servodata.aucroc.api.persistence.repository.file.FileRepository;
import net.servodata.aucroc.api.service.file.FileDTO;
import net.servodata.aucroc.api.service.file.FileQueryService;

@Named
@Singleton
@OsgiServiceProvider(classes = FileQueryService.class)
@Transactional
public class FileQueryServiceImpl implements FileQueryService
{
    @Inject
    @OsgiService
    private FileRepository repository;

    public void setRepository(FileRepository repository)
    {
        this.repository = repository;
    }

    @Override
    public FileDTO findById(long id)
    {
        return FileMapper.map(repository.findById(id));
    }

    @Override
    public List<FileDTO> findAll()
    {
        return null;
    }

    @Override
    public List<FileDTO> findAll(int limit, int offset)
    {
        return null;
    }
}
