package net.servodata.aucroc.service.file;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.FileMapper;
import net.servodata.aucroc.api.persistence.repository.file.FileEntity;
import net.servodata.aucroc.api.persistence.repository.file.FileRepository;
import net.servodata.aucroc.api.service.file.FileCRUDService;
import net.servodata.aucroc.api.service.file.FileDTO;

@Named
@Singleton
@OsgiServiceProvider(classes = FileCRUDService.class)
@Transactional
public class FileCRUDServiceImpl implements FileCRUDService
{
    private static final Logger LOG = LogManager.getLogger(FileCRUDServiceImpl.class);

    @Inject
    @OsgiService
    private FileRepository repository;

    public void setRepository(FileRepository repository)
    {
        this.repository = repository;
    }

    @Override
    public FileDTO create(FileDTO dto)
    {
        final FileEntity entity = FileMapper.map(dto);

        LOG.info("Persisting " + dto.toString() + " as entity " + entity.toString());

        final long insertId = repository.insert(entity);
        return findById(insertId);
    }

    @Override
    public void update(FileDTO dto)
    {

    }

    @Override
    public void delete(FileDTO dto)
    {

    }

    @Override
    public void deleteById(long id)
    {

    }

    public FileDTO findById(long id)
    {
        return FileMapper.map(repository.findById(id));
    }

}
