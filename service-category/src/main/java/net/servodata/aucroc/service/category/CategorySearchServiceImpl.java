package net.servodata.aucroc.service.category;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.CategoryMapper;
import net.servodata.aucroc.api.persistence.SortOrder;
import net.servodata.aucroc.api.persistence.repository.category.CategoryFilter;
import net.servodata.aucroc.api.persistence.repository.category.CategoryRepository;
import net.servodata.aucroc.api.persistence.repository.category.CategorySort;
import net.servodata.aucroc.api.service.category.CategoryDTO;
import net.servodata.aucroc.api.service.category.CategorySearchService;

@Named
@Singleton
@OsgiServiceProvider(classes = {CategorySearchService.class})
@Transactional
public class CategorySearchServiceImpl implements CategorySearchService
{
    private static final Logger LOG = LogManager.getLogger(CategorySearchServiceImpl.class);

    @Inject
    @OsgiService
    private CategoryRepository repository;

    @Override
    public List<CategoryDTO> search(String text)
    {
        final CategoryFilter filter = new CategoryFilter();
        filter.setAvailable(true);
        filter.setFulltext(text);

        final CategorySort sort = new CategorySort();
        sort.setOrder(SortOrder.ASC);
        sort.setName(SortOrder.ASC);

        return repository.filter(filter, sort).stream()
                .map(CategoryMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> search(String text, int limit)
    {
        return search(text, limit, 0);
    }

    @Override
    public List<CategoryDTO> search(String text, int limit, int offset)
    {
        final CategoryFilter filter = new CategoryFilter();
        filter.setAvailable(true);
        filter.setFulltext(text);

        final CategorySort sort = new CategorySort();
        sort.setOrder(SortOrder.ASC);
        sort.setName(SortOrder.ASC);

        return repository.filter(limit, offset, filter, sort).stream()
                .map(CategoryMapper::map)
                .collect(Collectors.toList());
    }
}
