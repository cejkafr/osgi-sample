package net.servodata.aucroc.service.category;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.CategoryMapper;
import net.servodata.aucroc.api.persistence.SortOrder;
import net.servodata.aucroc.api.persistence.repository.category.CategoryFilter;
import net.servodata.aucroc.api.persistence.repository.category.CategoryRepository;
import net.servodata.aucroc.api.persistence.repository.category.CategorySort;
import net.servodata.aucroc.api.service.category.CategoryQueryService;
import net.servodata.aucroc.api.service.category.CategoryDTO;

@Named
@Singleton
@OsgiServiceProvider(classes = {CategoryQueryService.class})
@Transactional
public class CategoryQueryServiceImpl implements CategoryQueryService
{
    private static final Logger LOG = LogManager.getLogger(CategoryQueryServiceImpl.class);

    @Inject
    @OsgiService
    private CategoryRepository repository;

    @Override
    public CategoryDTO findById(long id)
    {
        return CategoryMapper.map(repository.findById(id));
    }

    @Override
    public List<CategoryDTO> findAll()
    {
        CategoryFilter filter = new CategoryFilter();
        filter.setAvailable(true);

        CategorySort sort = new CategorySort();
        sort.setOrder(SortOrder.ASC);
        sort.setName(SortOrder.ASC);

        return repository.filter(filter, sort).stream()
                .map(CategoryMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> findAll(int limit, int offset)
    {
        CategoryFilter filter = new CategoryFilter();
        filter.setAvailable(true);

        CategorySort sort = new CategorySort();
        sort.setOrder(SortOrder.ASC);
        sort.setName(SortOrder.ASC);

        return repository.filter(limit, offset, filter, sort).stream()
                .map(CategoryMapper::map)
                .collect(Collectors.toList());
    }
}
