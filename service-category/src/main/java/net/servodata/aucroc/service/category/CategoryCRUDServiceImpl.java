package net.servodata.aucroc.service.category;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ops4j.pax.cdi.api.OsgiService;
import org.ops4j.pax.cdi.api.OsgiServiceProvider;

import net.servodata.aucroc.api.mapper.CategoryMapper;
import net.servodata.aucroc.api.persistence.SortOrder;
import net.servodata.aucroc.api.persistence.repository.category.CategoryEntity;
import net.servodata.aucroc.api.persistence.repository.category.CategoryFilter;
import net.servodata.aucroc.api.persistence.repository.category.CategoryRepository;
import net.servodata.aucroc.api.persistence.repository.category.CategorySort;
import net.servodata.aucroc.api.service.category.CategoryCRUDService;
import net.servodata.aucroc.api.service.category.CategoryDTO;

@Named
@Singleton
@OsgiServiceProvider(classes = {CategoryCRUDService.class})
@Transactional
public class CategoryCRUDServiceImpl implements CategoryCRUDService
{
    private static final Logger LOG = LogManager.getLogger(CategoryCRUDServiceImpl.class);

    @Inject
    @OsgiService
    private CategoryRepository repository;

    @PostConstruct
    public void activate()
    {
        LOG.info("Running PostConstruct.");

        CategoryDTO dto;

        dto = new CategoryDTO();
        dto.setName("Category 1");
        dto.setDescription("Description of category 1");
        dto.setOrder(1);
        dto.setAvailable(true);
        create(dto);

        dto = new CategoryDTO();
        dto.setName("Category 2");
        dto.setDescription("Description of category 2");
        dto.setOrder(2);
        dto.setAvailable(true);
        create(dto);

        dto = new CategoryDTO();
        dto.setName("Category 3");
        dto.setDescription("Description of category 3");
        dto.setOrder(3);
        dto.setAvailable(true);
        create(dto);

        CategoryFilter filter = new CategoryFilter();
        filter.setAvailable(true);

        CategorySort sort = new CategorySort();
        sort.setOrder(SortOrder.ASC);
        sort.setName(SortOrder.ASC);

        int num = repository.filter(filter, sort).size();

        LOG.info("Found " + num + " standard items.");
    }

    @Override
    public CategoryDTO create(CategoryDTO dto)
    {
        final CategoryEntity entity = CategoryMapper.map(dto);

        LOG.info("Persisting " + dto.toString() + " as entity " + entity.toString());

        final long insertId = repository.insert(entity);
        return findById(insertId);
    }

    @Override
    public void update(CategoryDTO dto)
    {
        CategoryEntity entity = repository.findById(dto.getId());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setOrder(dto.getOrder());
        entity.setAvailable(dto.isAvailable());
        repository.update(entity);

        dto.setUpdatedAt(entity.getUpdatedAt().toInstant());
    }

    @Override
    public void delete(CategoryDTO dto)
    {
        repository.deleteById(dto.getId());
    }

    @Override
    public void deleteById(long id)
    {
        repository.deleteById(id);
    }

    private CategoryDTO findById(long id)
    {
        return CategoryMapper.map(repository.findById(id));
    }

    public void setRepository(CategoryRepository repository)
    {
        this.repository = repository;
    }
}
