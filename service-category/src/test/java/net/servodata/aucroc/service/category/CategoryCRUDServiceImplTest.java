package net.servodata.aucroc.service.category;

import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import net.servodata.aucroc.api.persistence.repository.category.CategoryEntity;
import net.servodata.aucroc.api.persistence.repository.category.CategoryRepository;
import net.servodata.aucroc.api.service.category.CategoryCRUDService;
import net.servodata.aucroc.api.service.category.CategoryDTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CategoryCRUDServiceImplTest
{
    private static final Logger LOG = LogManager.getLogger(CategoryCRUDServiceImplTest.class);

    private CategoryRepository repository;
    private CategoryCRUDService service;

    @Before
    public void setUp() throws Exception
    {
        repository = mock(CategoryRepository.class);
        service = new CategoryCRUDServiceImpl();

        ((CategoryCRUDServiceImpl) service).setRepository(repository);
    }

    @Test
    public void create()
    {
        final CategoryDTO createDTO = createDTO();

        when(repository.insert(any(CategoryEntity.class))).thenAnswer(invocation ->
        {
            final CategoryEntity entity = (CategoryEntity) invocation.getArguments()[0];

            assertEquals(createDTO.getName(), entity.getName());
            assertEquals(createDTO.getDescription(), entity.getDescription());
            assertEquals(createDTO.getOrder(), entity.getOrder());

            return 10L;
        });

        when(repository.findById(10l)).thenAnswer(invocation ->
        {
            final Long id = (Long) invocation.getArguments()[0];
            return createEntity(id);
        });


        final CategoryDTO createdDTO = service.create(createDTO);

        assertEquals(10l, createdDTO.getId());
        assertEquals(createDTO.getName(), createdDTO.getName());
        assertEquals(createDTO.getDescription(), createdDTO.getDescription());
        assertEquals(createDTO.getOrder(), createdDTO.getOrder());
        assertNotNull(createdDTO.getCreatedAt());
        assertNotNull(createdDTO.getUpdatedAt());
    }

    private CategoryDTO createDTO()
    {
        return new CategoryDTO().setName("Test DTO")
                .setDescription("Test DTO Description")
                .setOrder(101);
    }

    private CategoryEntity createEntity(Long id)
    {
        CategoryEntity entity = new CategoryEntity();
        entity.setId(id);
        entity.setName("Test DTO");
        entity.setDescription("Test DTO Description");
        entity.setOrder(101);
        entity.setAvailable(true);
        entity.setDeleted(false);
        entity.setCreatedAt(Calendar.getInstance());
        entity.setUpdatedAt(Calendar.getInstance());
        return entity;
    }
}
